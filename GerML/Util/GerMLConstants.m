//
//  GerMLConstants.m
//  GerML
//
//  Created by Ger Gonzalez on 29/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import "GerMLConstants.h"

@implementation GerMLConstants

// Strings

NSString *const kGerMLResults = @"results";
NSString *const kGerMLId = @"id";
NSString *const kGerMLTitle = @"title";
NSString *const kGerMLThumbnail = @"thumbnail";
NSString *const kGerMLPrice = @"price";
NSString *const kGerMLCurrencyId = @"currency_id";
NSString *const kGerMLSeller = @"seller";
NSString *const kGerMLAvailableQuantity = @"available_quantity";
NSString *const kGerMLSoldQuantity = @"sold_quantity";
NSString *const kGerMLCondition = @"condition";
NSString *const kGerMLInstallments = @"installments";
NSString *const kGerMLAddress = @"address";
NSString *const kGerMLShipping = @"shipping";
NSString *const kGerMLSellerAddress = @"seller_address";
NSString *const kGerMLAttributes = @"attributes";
NSString *const kGerMLCategoryId = @"categoryId";
NSString *const kGerMLCatalogProductId = @"catalogProductId";
NSString *const kGerMLReviews = @"reviews";
NSString *const kGerMLPowerSellerStatus = @"power_seller_status";
NSString *const kGerMLState = @"state";
NSString *const kGerMLCountry = @"country";
NSString *const kGerMLCity = @"city";
NSString *const kGerMLNameValue = @"name";
NSString *const kGerMLPlainTextKey = @"plain_text";
NSString *const kGerMLBrand = @"BRAND";
NSString *const kGerMLItemCondition = @"ITEM_CONDITION";
NSString *const kGerMLModel = @"MODEL";
NSString *const kGerMLValueNameKey = @"value_name";

// Segues

NSString *const kGerMLSegueProductDetail        = @"showProductDetail";

// Cell Identifier

NSString *const kGerMLCellIdentifierProduct      = @"cellProductIdentifier";

@end
