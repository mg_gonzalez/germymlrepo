//
//  GerMLConstants.h
//  GerML
//
//  Created by Ger Gonzalez on 29/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GerMLConstants : NSObject

// Strings

extern NSString *const kGerMLResults;
extern NSString *const kGerMLId;
extern NSString *const kGerMLTitle;
extern NSString *const kGerMLThumbnail;
extern NSString *const kGerMLPrice;
extern NSString *const kGerMLCurrencyId;
extern NSString *const kGerMLSeller;
extern NSString *const kGerMLAvailableQuantity;
extern NSString *const kGerMLSoldQuantity;
extern NSString *const kGerMLCondition;
extern NSString *const kGerMLInstallments;
extern NSString *const kGerMLAddress;
extern NSString *const kGerMLShipping;
extern NSString *const kGerMLSellerAddress;
extern NSString *const kGerMLAttributes;
extern NSString *const kGerMLCategoryId;
extern NSString *const kGerMLCatalogProductId;
extern NSString *const kGerMLReviews;
extern NSString *const kGerMLPowerSellerStatus;
extern NSString *const kGerMLState;
extern NSString *const kGerMLCountry;
extern NSString *const kGerMLCity;
extern NSString *const kGerMLNameValue;
extern NSString *const kGerMLPlainTextKey;
extern NSString *const kGerMLCatalogProductId;
extern NSString *const kGerMLReviews;
extern NSString *const kGerMLBrand;
extern NSString *const kGerMLItemCondition;
extern NSString *const kGerMLModel;
extern NSString *const kGerMLValueNameKey;

// Segues

extern NSString *const kGerMLSegueProductDetail;

// Cell Indentifier

extern NSString *const kGerMLCellIdentifierProduct;


@end
