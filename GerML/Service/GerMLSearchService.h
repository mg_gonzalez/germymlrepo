//
//  GerMLSearchService.h
//  GerML
//
//  Created by Ger Gonzalez on 22/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GerMLHTTPHelper.h"

@interface GerMLSearchService : NSObject

/**
 * product_search
 
 *  Busqueda de producto
 * @"https://api.mercadolibre.com/sites/MLA/search?q=iphone"
 */
+(void)productSearch:(NSString *)product
      successBlock:(GerMLHTTPOperationCompletionBlock)completionBlock
      failureBlock:(GerMLHTTPOperationFailureBlock)failureBlock;

/**
 * product_description
 
 *  Descripcion completa de producto
 * @"https://api.mercadolibre.com/items/MLA682627325/description"
 */
+(void)productDescription:(NSString *)productId
       successBlock:(GerMLHTTPOperationCompletionBlock)completionBlock
       failureBlock:(GerMLHTTPOperationFailureBlock)failureBlock;

@end
