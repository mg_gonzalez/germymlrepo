//
//  GerMLSearchService.m
//  GerML
//
//  Created by Ger Gonzalez on 22/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import "GerMLSearchService.h"

// OPERATION

#define OPERATION_SITES_ARG      @"sites/MLA/search"
#define PARAM_BUSQUEDA_ITEM      @"q"

#define OPERATION_ITEM_DESCRIPTION                  @"items/id_item/description"
#define PARAM_ITEM_ID                               @"id_item"

@implementation GerMLSearchService

+(void)productSearch:(NSString *)product
      successBlock:(GerMLHTTPOperationCompletionBlock)completionBlock
      failureBlock:(GerMLHTTPOperationFailureBlock)failureBlock {
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:product forKey:PARAM_BUSQUEDA_ITEM];

    [[GerMLHTTPHelper instance] httpOperation:OPERATION_SITES_ARG method:METHOD_GET withParameters:params completionBlock:^(NSArray *responseObject) {
        completionBlock(responseObject);
    } failureBlock:^(NSError *error) {
        failureBlock(error);
    }];
}

+(void)productDescription:(NSString *)productId successBlock:(GerMLHTTPOperationCompletionBlock)completionBlock failureBlock:(GerMLHTTPOperationFailureBlock)failureBlock{
    
    NSString *operation = OPERATION_ITEM_DESCRIPTION;
    NSString *operationWithItemId = [operation stringByReplacingOccurrencesOfString:PARAM_ITEM_ID withString:productId];
    
    [[GerMLHTTPHelper instance] httpOperation:operationWithItemId method:METHOD_GET withParameters:nil completionBlock:^(NSArray *responseObject) {
        completionBlock(responseObject);
    } failureBlock:^(NSError *error) {
        failureBlock(error);
    }];
}

@end
