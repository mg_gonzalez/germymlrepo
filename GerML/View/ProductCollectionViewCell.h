//
//  ProductCollectionViewCell.h
//  GerML
//
//  Created by Ger Gonzalez on 23/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCollectionViewCell : UICollectionViewCell
//Outlets

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;

@end
