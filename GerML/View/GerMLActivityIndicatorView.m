//
//  GerMLActivityIndicatorView.m
//  GerML
//
//  Created by Ger Gonzalez on 22/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import "GerMLActivityIndicatorView.h"

@implementation GerMLActivityIndicatorView

#pragma mark - Initialization

+(instancetype) instance {
    static GerMLActivityIndicatorView *loadingView = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        loadingView = [[self alloc] initWithFrame:[UIScreen mainScreen].bounds];
    });
    return loadingView;
}

-(instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor blackColor]];
        [self setAlpha:0.6f];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2)];
        [self.activityIndicator setColor:[UIColor yellowColor]];
        
        [self addSubview:self.activityIndicator];
    }
    return self;
}

#pragma mark - Overrides

-(void) didMoveToSuperview {
    if ([self superview]) {
        [self.activityIndicator startAnimating];
    } else {
        [self.activityIndicator stopAnimating];
    }
}

@end
