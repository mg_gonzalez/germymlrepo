//
//  GerMLActivityIndicatorView.h
//  GerML
//
//  Created by Ger Gonzalez on 22/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GerMLActivityIndicatorView : UIView

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

#pragma mark - Initialization

+ (instancetype) instance;
- (instancetype) initWithFrame:(CGRect)frame;

@end
