//
//  ProductDetailModel.h
//  GerML
//
//  Created by Ger Gonzalez on 24/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ProductDetailModel : NSObject

@property (strong, nonatomic) NSString *idProducto;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *thumbnail;
@property (strong, nonatomic) UIImage *productImage;
@property (strong, nonatomic) NSString *site_id;
@property (strong, nonatomic) NSNumber *price;
@property (strong, nonatomic) NSString *currency_id;
@property (strong, nonatomic) NSDictionary *seller;
@property (strong, nonatomic) NSNumber *available_quantity;
@property (strong, nonatomic) NSNumber *sold_quantity;
@property (strong, nonatomic) NSString *condition;
@property (strong, nonatomic) NSDictionary *installments;
@property (strong, nonatomic) NSDictionary *address;
@property (strong, nonatomic) NSDictionary *shipping;
@property (strong, nonatomic) NSDictionary *seller_address;
@property (strong, nonatomic) NSArray *attributes;
@property (strong, nonatomic) NSString *categoryId;
@property (strong, nonatomic) NSString *catalogProductId;
@property (strong, nonatomic) NSDictionary *reviews;

@end
