//
//  GerMLHTTPHelper.h
//  GerML
//
//  Created by Ger Gonzalez on 22/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>

#define METHOD_GET  @"GET"
#define METHOD_POST @"POST"

typedef void (^GerMLHTTPOperationCompletionBlock)(NSArray *responseObject);
typedef void (^GerMLHTTPOperationFailureBlock)(NSError *error);

@interface GerMLHTTPHelper : NSObject

#pragma mark - Initialization

+(instancetype)instance;

#pragma mark - Public

-(void)httpOperation:(NSString *)operation
              method:(NSString *)method
      withParameters:(NSDictionary *)parameters
     completionBlock:(GerMLHTTPOperationCompletionBlock)completionBlock
        failureBlock:(GerMLHTTPOperationFailureBlock)failureBlock;

@end
