//
//  GerMLHTTPHelper.m
//  GerML
//
//  Created by Ger Gonzalez on 22/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import "GerMLHTTPHelper.h"
#import <AFNetworking.h>
#import <AFHTTPSessionManager.h>

#define BASE_URL    @"https://api.mercadolibre.com/"
#define TEXT_PLAIN   @"text/plain"

@interface GerMLHTTPHelper()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;
@property (nonatomic, strong) NSOperationQueue     *operationQueue;

@end

@implementation GerMLHTTPHelper

+(instancetype)instance {
    static GerMLHTTPHelper *httpHelper;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        httpHelper = [[GerMLHTTPHelper alloc] init];
    });
    return httpHelper;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configureSessionManager];
        [self setupOperationQueue];
    }
    return self;
}


#pragma mark - Public

-(void)httpOperation:(NSString *)operation
              method:(NSString *)method
      withParameters:(NSDictionary *)parameters
     completionBlock:(GerMLHTTPOperationCompletionBlock)completionBlock
        failureBlock:(GerMLHTTPOperationFailureBlock)failureBlock
{
    
    NSString *URL = [BASE_URL stringByAppendingString:operation];
    
    if ([method isEqualToString:METHOD_GET])
    {
        [self GET:URL withParameters:parameters completionBlock:completionBlock failureBlock:failureBlock];
    }
    else if ([method isEqualToString:METHOD_POST])
    {
        [self POST:URL withParameters:parameters completionBlock:completionBlock failureBlock:failureBlock];
    }
}

#pragma mark - Internal

-(void)configureSessionManager
{
    self.sessionManager = [AFHTTPSessionManager manager];
    self.sessionManager.responseSerializer.acceptableContentTypes = [self.sessionManager.responseSerializer.acceptableContentTypes setByAddingObject:TEXT_PLAIN];
}

-(void)setupOperationQueue
{
    self.operationQueue = [NSOperationQueue new];
}

-(void)GET:(NSString *)URL withParameters:(NSDictionary *)parameters completionBlock:(GerMLHTTPOperationCompletionBlock)completionBlock failureBlock:(GerMLHTTPOperationFailureBlock)failureBlock
{
    __weak GerMLHTTPHelper *helper = self;
    NSOperation *operation = [NSOperation new];
    operation.completionBlock = ^{
        [helper.sessionManager GET:URL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            completionBlock(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [helper handleServiceError:task error:error completionBlock:completionBlock failureBlock:failureBlock];
        }];
    };
    
    [self.operationQueue addOperation:operation];
}

-(void)POST:(NSString *)URL withParameters:(NSDictionary *)parameters completionBlock:(GerMLHTTPOperationCompletionBlock)completionBlock failureBlock:(GerMLHTTPOperationFailureBlock)failureBlock
{
    __weak GerMLHTTPHelper *helper = self;
    NSOperation *operation = [NSOperation new];
    operation.completionBlock = ^{
        [helper.sessionManager POST:URL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            completionBlock(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [helper handleServiceError:task error:error completionBlock:completionBlock failureBlock:failureBlock];
        }];
    };
    
    [self.operationQueue addOperation:operation];
}

-(void)handleServiceError:(NSURLSessionDataTask *) task error:(NSError *)error completionBlock:(GerMLHTTPOperationCompletionBlock)completionBlock failureBlock:(GerMLHTTPOperationFailureBlock)failureBlock {
    
    // Some calls will execute the failure block upon success.
    // Check status code and act accordingly.
    
    if (!task){
        failureBlock(error);
        return;
    }
    
    NSHTTPURLResponse* response = (NSHTTPURLResponse*)task.response;
    if (response.statusCode == 200){
        completionBlock(nil);
    }else{
        failureBlock(error);
    }
}

@end
