//
//  AppDelegate.h
//  GerML
//
//  Created by Ger Gonzalez on 18/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

