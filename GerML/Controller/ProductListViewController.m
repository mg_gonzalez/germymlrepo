//
//  ProductListViewController.m
//  GerML
//
//  Created by Ger Gonzalez on 18/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import "ProductListViewController.h"
#import "GerMLSearchService.h"
#import "GerMLActivityIndicatorView.h"
#import "ProductCollectionViewCell.h"
#import "ProductDetailViewController.h"
#import "ProductDetailModel.h"
#import <UIImageView+AFNetworking.h>
#import "GerMLConstants.h"

@interface ProductListViewController ()

//LOADING INDICATOR
@property (nonatomic, strong) GerMLActivityIndicatorView *activityIndicatorView;

//Outlets
@property (weak, nonatomic) IBOutlet UISearchBar *productSearchBar;
@property (weak, nonatomic) IBOutlet UICollectionView *productCollectionView;

//Properties
@property (strong, nonatomic) NSArray *resultsArray;
@property (strong, nonatomic) NSArray<ProductDetailModel*> *collectionViewItemsArray;
@property (strong, nonatomic) UIImage *productImage;
@property (strong, nonatomic) ProductDetailModel* selectedProduct;

//Handle Errors
@property (nonatomic, assign) BOOL                    promptDisplayed;
@property (nonatomic, strong) UIAlertController       *userPrompt;
@property (nonatomic, strong) UIAlertAction           *primaryAction;
@property (nonatomic, strong) UIAlertAction           *secondaryAction;

@end

NSString *const kGerMLDefaultPrimaryAction     = @"Aceptar";
NSString *const kGerMLDefaultSecondaryAction   = @"Cancelar";
NSString *const kGerMLErrorDescriptionForSearchCall = @"Could not search list of products";

@implementation ProductListViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _activityIndicatorView = [GerMLActivityIndicatorView instance];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Set delegates
    self.productSearchBar.delegate = self;
    self.productCollectionView.delegate = self;
    self.productCollectionView.dataSource = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

/**
 It get and returns the full description of the specific product.
 
 - Parameter searchedText: This string is obtained in the search bar and is used to do the call to the api.
 
 - Get: The all description for the searched product and mapped on the ProductDetailModel.
 
 */
-(void)getData:(NSString*)searchedText{
    [self showActivityIndicator];
    
    __weak typeof(self) weakSelf = self;
    
    [GerMLSearchService productSearch:searchedText successBlock:^(NSArray *responseObject) {
        [weakSelf hideActivityIndicator];
        
        //TODO handleo busqueda de motos/autos/camionetas a traves de los filtros disponibles
        
        weakSelf.resultsArray = [responseObject valueForKey:kGerMLResults];
        NSMutableArray *products = [NSMutableArray new];
        
        for (NSDictionary *results in weakSelf.resultsArray) {
            @try {
                ProductDetailModel *product = [ProductDetailModel new];
                
                if ([results objectForKey:kGerMLId] != [NSNull null]) {
                    product.idProducto = [results objectForKey:kGerMLId] ;
                }
                
                if ([results objectForKey:kGerMLTitle] != [NSNull null]) {
                    product.title = [results objectForKey:kGerMLTitle] ;
                }
                
                if ([results objectForKey:kGerMLThumbnail] != [NSNull null]) {
                    product.thumbnail = [results objectForKey:kGerMLThumbnail] ;
                }
                
                if ([results objectForKey:kGerMLPrice] != [NSNull null]) {
                    product.price = [results objectForKey:kGerMLPrice] ;
                }
                
                if ([results objectForKey:kGerMLCurrencyId] != [NSNull null]) {
                    product.currency_id = [results objectForKey:kGerMLCurrencyId] ;
                }
                
                if ([results objectForKey:kGerMLSeller] != [NSNull null]) {
                    product.seller = [results objectForKey:kGerMLSeller] ;
                }
                
                if ([results objectForKey:kGerMLAvailableQuantity] != [NSNull null]) {
                    product.available_quantity = [results objectForKey:kGerMLAvailableQuantity] ;
                }
                
                if ([results objectForKey:kGerMLSoldQuantity] != [NSNull null]) {
                    product.sold_quantity = [results objectForKey:kGerMLSoldQuantity] ;
                }
                
                if ([results objectForKey:kGerMLCondition] != [NSNull null]) {
                    product.condition = [results objectForKey:kGerMLCondition] ;
                }
                
                if ([results objectForKey:kGerMLInstallments] != [NSNull null]) {
                    product.installments = [results objectForKey:kGerMLInstallments] ;
                }
                
                if ([results objectForKey:kGerMLAddress] != [NSNull null]) {
                    product.address = [results objectForKey:kGerMLAddress] ;
                }
                
                if ([results objectForKey:kGerMLShipping] != [NSNull null]) {
                    product.shipping = [results objectForKey:kGerMLShipping] ;
                }
                
                if ([results objectForKey:kGerMLSellerAddress] != [NSNull null]) {
                    product.seller_address = [results objectForKey:kGerMLSellerAddress] ;
                }
                
                if ([results objectForKey:kGerMLAttributes] != [NSNull null]) {
                    product.attributes = [results objectForKey:kGerMLAttributes] ;
                }
                
                if ([results objectForKey:kGerMLCategoryId] != [NSNull null]) {
                    product.categoryId = [results objectForKey:kGerMLCategoryId] ;
                }
                
                if ([results objectForKey:kGerMLCatalogProductId] != [NSNull null]) {
                    product.catalogProductId = [results objectForKey:kGerMLCatalogProductId] ;
                }
                
                if ([results objectForKey:kGerMLReviews] != [NSNull null]) {
                    product.reviews = [results objectForKey:kGerMLReviews] ;
                }
                
                [products addObject:product];
                
            } @catch (NSException *exception) {
                continue;
            }
        }
        
        weakSelf.collectionViewItemsArray = products;
        [weakSelf.productCollectionView reloadData];
        
        //Always navigate to the top in a new search when results are obtained
        if (weakSelf.collectionViewItemsArray.count >0) {
            [weakSelf.productCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                                                   atScrollPosition:UICollectionViewScrollPositionTop
                                                        animated:YES];
        }
    } failureBlock:^(NSError *error) {
        [weakSelf hideActivityIndicator];
        [weakSelf handleError:kGerMLErrorDescriptionForSearchCall];
    }];
}

#pragma mark - UISearchBarDelegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    NSString *searchText = searchBar.text;
    [searchBar endEditing:YES];
    [self getData:searchText];
}

#pragma mark - UICollectiomViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return [self.collectionViewItemsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductDetailModel *product = self.collectionViewItemsArray[indexPath.item];
    
    ProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kGerMLCellIdentifierProduct forIndexPath:indexPath];
    
    cell.layer.cornerRadius = 15.0;
    
    cell.productTitleLabel.text = product.title;
    
    NSString *priceString = [NSString stringWithFormat:@"%@", product.price];
    cell.productPriceLabel.text = priceString;
    
    NSString *url = product.thumbnail;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    UIImage *placeholderImage = [UIImage imageNamed:@"placeholder"];
    
    __weak ProductCollectionViewCell *weakCell = cell;
    
    [cell.productImageView setImageWithURLRequest:request
                          placeholderImage:placeholderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       weakCell.productImageView.image = image;
                                       product.productImage = image;
                                       [weakCell layoutIfNeeded];
                                   } failure:nil];
    
    cell.currencyPriceLabel.text = product.currency_id;
    return cell;
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedProduct = self.collectionViewItemsArray[indexPath.item];
    [self performSegueWithIdentifier:kGerMLSegueProductDetail sender:self];
}


#pragma mark - UICollectionViewFlowLayoutDelegate

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{

    return 10.0;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{

    return 10.0;
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if ([[segue identifier] isEqualToString:kGerMLSegueProductDetail]){
        ProductDetailViewController *targetController = [segue destinationViewController];
        targetController.productDescriptionLabelText = self.selectedProduct.title;
        targetController.productDescriptionCurrencyLabelText = self.selectedProduct.currency_id ;
        targetController.productDescriptionPriceLabelText = [NSString stringWithFormat:@"%@", self.selectedProduct.price];
        targetController.productDescriptionImageViewImage = self.selectedProduct.productImage;
        targetController.seller = self.selectedProduct.seller;
        targetController.sellerAddress = self.selectedProduct.seller_address;
        targetController.productId = self.selectedProduct.idProducto;
        targetController.productAttributes = self.selectedProduct.attributes;
    }
}

#pragma mark - Handle Activity Indicator

/**
 It show activity indicator before the search call.
 */

-(void)showActivityIndicator {
    __weak ProductListViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[[UIApplication sharedApplication] delegate] window] addSubview:weakSelf.activityIndicatorView];
    });
}

/**
 It hide activity indicator after the search call.
 */

-(void)hideActivityIndicator {
    __weak ProductListViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.activityIndicatorView removeFromSuperview];
    });
}

#pragma mark - Handle error
/**
 It handle erros on the call. Is used in the failure block.
 */

-(void)handleError:(NSString *)errorMessage {
    
    self.userPrompt = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    
    __weak ProductListViewController *weakSelf = self;
    self.primaryAction = [UIAlertAction actionWithTitle:kGerMLDefaultPrimaryAction style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        weakSelf.promptDisplayed = NO;
        weakSelf.userPrompt = nil;
        weakSelf.primaryAction = nil;
    }];
    
    [self.userPrompt addAction:self.primaryAction];
    
    [self showPrompt];
}

/**
 It show prompt after handle error.
 */

-(void)showPrompt {
    
    if (self.userPrompt && !self.promptDisplayed)
    {
        __weak ProductListViewController *weakSelf = self;
        [self presentViewController:self.userPrompt animated:YES completion:^{
            weakSelf.promptDisplayed = YES;
        }];
    }
    
}
@end
