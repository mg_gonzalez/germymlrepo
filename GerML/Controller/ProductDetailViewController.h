//
//  ProductDetailViewController.h
//  GerML
//
//  Created by Ger Gonzalez on 24/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ProductDetailViewController : UIViewController

//DetailProductView
@property (weak, nonatomic) IBOutlet UIImageView *productDescriptionImageView;
@property (weak, nonatomic) IBOutlet UILabel     *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel     *productDescriptionCurrencyLabel;
@property (weak, nonatomic) IBOutlet UILabel     *productDescriptionPriceLabel;

@property (strong,nonatomic) NSString       *productDescriptionLabelText;
@property (strong,nonatomic) NSString       *productDescriptionCurrencyLabelText;
@property (strong,nonatomic) NSString       *productDescriptionPriceLabelText;
@property (strong,nonatomic) UIImage        *productDescriptionImageViewImage;
@property (strong,nonatomic) NSString       *productId;

//DetailSellerView
@property (weak, nonatomic) IBOutlet UILabel *sellerIdentifier;
@property (weak, nonatomic) IBOutlet UILabel *kindSeller;
@property (weak, nonatomic) IBOutlet UILabel *sellerStatus;
@property (weak, nonatomic) IBOutlet UILabel *sellerCountry;
@property (weak, nonatomic) IBOutlet UILabel *sellerState;
@property (weak, nonatomic) IBOutlet UILabel *sellerCity;

@property (strong, nonatomic) NSDictionary   *seller;
@property (strong, nonatomic) NSDictionary   *sellerAddress;

//FullDescription
@property (weak, nonatomic) IBOutlet UILabel *fullProductDescriptionLabel;
@property (strong,nonatomic) NSString        *fullDescriptionProduct;

//Product Attributtes
@property (weak, nonatomic) IBOutlet UILabel *productBrandLabel;
@property (weak, nonatomic) IBOutlet UILabel *productModelLabel;
@property (weak, nonatomic) IBOutlet UILabel *productConditionLabel;

@property (strong, nonatomic) NSArray *productAttributes;

@end
