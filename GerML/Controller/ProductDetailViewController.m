//
//  ProductDetailViewController.m
//  GerML
//
//  Created by Ger Gonzalez on 24/7/18.
//  Copyright © 2018 Ger Gonzalez. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "GerMLSearchService.h"
#import "GerMLConstants.h"

@interface ProductDetailViewController ()

//Handle Errors
@property (nonatomic, assign) BOOL                    promptDisplayed;
@property (nonatomic, strong) UIAlertController       *userPrompt;
@property (nonatomic, strong) UIAlertAction           *primaryAction;
@property (nonatomic, strong) UIAlertAction           *secondaryAction;

@end

NSString *const KGerMLNoSummary  = @"No summary";
NSString *const kGerMLDefaultPrimaryActionProductDetail     = @"Aceptar";
NSString *const kGerMLDefaultSecondaryActionProductDetail   = @"Cancelar";
NSString *const kGerMLErrorDescriptionFullDescriptionCall   = @"Full Description not found";

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.productDescriptionLabel.text = self.productDescriptionLabelText;
    self.productDescriptionCurrencyLabel.text = self.productDescriptionCurrencyLabelText;
    self.productDescriptionPriceLabel.text = self.productDescriptionPriceLabelText;
    self.productDescriptionImageView.image = self.productDescriptionImageViewImage;
    
    // Do any additional setup after loading the view.
    [self getSellerInfo];
    [self getSellerAddressInfo];
    [self getProductDescription:self.productId];
    [self getProductAttributes];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

/**
 It gets the seller info of the product.
 */

-(void) getSellerInfo{
    for (NSString *key in self.seller) {
        if ([key isEqualToString:kGerMLPowerSellerStatus]) {
            @try {
                if ([self.seller objectForKey:key] !=[NSNull null] ) {
                    self.sellerStatus.text = [self.seller objectForKey:key];
                } else {
                    self.sellerStatus.text = KGerMLNoSummary;
                }
            } @catch (NSException *exception) {
                continue;
            }
        }
    }
}

/**
 It gets the seller address info of the product.
 */

-(void) getSellerAddressInfo{
    
    for (NSString *key in self.sellerAddress) {
        @try {
            if ([key isEqualToString:kGerMLState]) {
                if ([self.sellerAddress objectForKey:key] !=[NSNull null] ) {
                    NSDictionary *state = [self.sellerAddress objectForKey:key];
                    for (NSString *key in state) {
                        if ([key isEqualToString:kGerMLNameValue]) {
                            self.sellerState.text = [state objectForKey:key];
                        } else {
                            self.sellerState.text = KGerMLNoSummary;
                        }
                    }
                }
            }
            
            if ([key isEqualToString:kGerMLCountry]) {
                if ([self.sellerAddress objectForKey:key] !=[NSNull null] ) {
                    NSDictionary *country = [self.sellerAddress objectForKey:key];
                    for (NSString *key in country) {
                        if ([key isEqualToString:kGerMLNameValue]) {
                            self.sellerCountry.text = [country objectForKey:key];
                        } else {
                            self.sellerCountry.text = KGerMLNoSummary;
                        }
                    }
                }
            }
            
            if ([key isEqualToString:kGerMLCity]) {
                if ([self.sellerAddress objectForKey:key] !=[NSNull null] ) {
                    NSDictionary *city = [self.sellerAddress objectForKey:key];
                    for (NSString *key in city) {
                        if ([key isEqualToString:kGerMLNameValue]) {
                            self.sellerCity.text = [city objectForKey:key];
                        } else {
                            self.sellerCity.text = KGerMLNoSummary;
                        }
                    }
                }
            }
         }
            @catch (NSException *exception) {
                continue;
         }
    }
}

/**
 It get and returns the full description of the specific product.
 
 - Parameter productId: This id is obtained in the search call a and is passed to this view controller into the segue.
 
 - Get: the full description as a string value.
 
 */

-(void)getProductDescription:(NSString*)productId{
    
    __weak typeof(self) weakSelf = self;
    __block NSString *fullDescription = [NSString new];
    [GerMLSearchService productDescription:productId successBlock:^(NSArray *responseObject) {
        for (NSString *key in responseObject) {
            if ([key isEqualToString:kGerMLPlainTextKey]) {
                fullDescription = [responseObject valueForKey:key];
                dispatch_async(dispatch_get_main_queue(), ^{
                    fullDescription = [responseObject valueForKey:key];
                    weakSelf.fullProductDescriptionLabel.text = fullDescription;
                });
            }
        }        
    } failureBlock:^(NSError *error) {
        [weakSelf handleError:kGerMLErrorDescriptionFullDescriptionCall];
    }];
}

/**
 It gets the attributes of the product.
 */

-(void)getProductAttributes{
    
    for (NSDictionary *attributes in self.productAttributes) {
        for (NSString *key in attributes) {
            @try{

                if ([[attributes objectForKey:key]  isEqual: kGerMLBrand]) {
                    NSLog(@"%@",[attributes objectForKey:kGerMLValueNameKey]);
                    
                    if ([attributes objectForKey:kGerMLValueNameKey] !=[NSNull null]) {
                        self.productBrandLabel.text = [attributes objectForKey:kGerMLValueNameKey];
                    }else {
                        self.productBrandLabel.text = KGerMLNoSummary;
                    }
                }
                
                if ([[attributes objectForKey:key]  isEqual: kGerMLItemCondition]) {
                    NSLog(@"%@",[attributes objectForKey:kGerMLValueNameKey]);
                    
                    if ([attributes objectForKey:kGerMLValueNameKey] !=[NSNull null]) {
                        self.productConditionLabel.text = [attributes objectForKey:kGerMLValueNameKey];
                    }else {
                        self.productConditionLabel.text = KGerMLNoSummary;
                    }
                }
                
                if ([[attributes objectForKey:key]  isEqual: kGerMLModel]) {
                    NSLog(@"%@",[attributes objectForKey:kGerMLValueNameKey]);
                    
                    if ([attributes objectForKey:kGerMLValueNameKey] !=[NSNull null]) {
                        self.productModelLabel.text = [attributes objectForKey:kGerMLValueNameKey];
                    }else {
                        self.productModelLabel.text = KGerMLNoSummary;
                    }
                }
                
            }@catch(NSException *exception){
                continue;
            }
        }
    }
}

#pragma mark - Handle error

/**
 It handle erros on the call. Is used in the failure block.
 */

-(void)handleError:(NSString *)errorMessage {
    
    self.userPrompt = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    
    __weak ProductDetailViewController *weakSelf = self;
    self.primaryAction = [UIAlertAction actionWithTitle:kGerMLDefaultPrimaryActionProductDetail style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        weakSelf.promptDisplayed = NO;
        weakSelf.userPrompt = nil;
        weakSelf.primaryAction = nil;
    }];
    
    [self.userPrompt addAction:self.primaryAction];
    
    [self showPrompt];
}

/**
 It show prompt after handle error.
 */

-(void)showPrompt {
    
    if (self.userPrompt && !self.promptDisplayed)
    {
        __weak ProductDetailViewController *weakSelf = self;
        [self presentViewController:self.userPrompt animated:YES completion:^{
            weakSelf.promptDisplayed = YES;
        }];
    }
    
}

@end
